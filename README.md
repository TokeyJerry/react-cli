# A TOOL FOR REACT & VUE CLI

## Installation

```sh
# install it globally
$ npm install -g react-cli
```

## Usage

```sh
# generate a new project
$ recl init

# input the name of your project & choose the type of your project 
```

## License

[MIT](LICENSE) &copy; [铁建文](http://platform.apptie.cn)