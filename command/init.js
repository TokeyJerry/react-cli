
'use strict';

//==================================【核心模块 shell 命令解析器】
const exec = require('child_process').exec;
//==================================【控制台提示文本】
const ora = require('ora');

module.exports = function (answers) {
	let gitAddress = '';

	switch (answers.type) {
		case 'react-cli for multipage application':
			gitAddress = 'git@gitlab.com:TokeyJerry/react-node.git';
			break;
		case 'vue-cli for singlepage application':
		  gitAddress = 'git@gitlab.com:TokeyJerry/react-node.git'
		  break;
	}

	let spinner = ora('downloading... (just take a break, these whole things may take a long time.)')
	spinner.start();

	exec(`git clone ${gitAddress} ${answers.name} --depth=1`, (error, stdout, stderr) => {
		if (error) {
			console.log(error);
			process.exit();
		}

		spinner.stop();

		let spinner2 = ora('installing...')
		spinner2.start();

		exec(`cd ${answers.name} && npm install`, (error, stdout, stderr) => {
			if (error) {
				console.log(error);
				process.exit();
			}

			spinner2.stop();
		})
	})
};

